﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ColoursOfDance.Enums
{
    public enum EnumDayOfWeek
    {
        LUNI,
        MARTI,
        MIERCURI,
        JOI,
        VINERI,
        SAMBATA,
        DUMINICA
    }
}