﻿$("#categoriesAccordion").accordion({
    collapsible: true
});

$("#eventDatePicker").datepicker({
    dateFormat: 'yy/mm/dd'
});

    google.charts.load("current", { packages: ["corechart"] });
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {

        $.post('Course/GetStudentsEnrolled', {}, function (data) {
            var tdata = new google.visualization.DataTable();

           
            tdata.addColumn('string', 'Curs');
            tdata.addColumn('number', 'Numar de studenti');

            for (var i = 0; i < data.length; i++) {
                tdata.addRow([data[i].Course, data[i].NumberOfStudents]);
            }

            
            var options = {
                title: 'Studenti',
                is3D:true
            };

            new google.visualization.PieChart(document.getElementById('piechart_3d')).
                draw(tdata, { title: "Grafic numar inscrisi", pieHole: 0.4 });
            
        });
    };
