﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ColoursOfDance.Models
{
    public class CategoryViewModel
    {
        public IEnumerable<Categories> Categories { get; set; }
        public Courses Course { get; set; }
    }
}