﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ColoursOfDance.Models
{
    public class CourseViewModel
    {
        public IEnumerable<Courses> Courses { get; set; }
        public IEnumerable<Students> Students { get; set; }
    }
}