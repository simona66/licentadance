﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ColoursOfDance.Models
{
    public class StudentViewModel
    {
        public Students Student { get; set; }
        
        [Display(Name="Cursuri")]
        public MultiSelectList Courses { get; set; }
        public string[] selectedCourses { get; set; }

    }
}