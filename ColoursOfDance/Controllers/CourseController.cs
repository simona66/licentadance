﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ColoursOfDance.Models;

namespace ColoursOfDance.Controllers
{
    [Authorize]
    public class CourseController : Controller
    {
        private ColoursOfDanceEntities1 db = new ColoursOfDanceEntities1();

        //
        // GET: /Admin/
        public ActionResult Index(int? id)
        {
            var courseView = new CourseViewModel();
            courseView.Courses = db.Courses
                .Include(c => c.Categories)
                .Include(c => c.Students)
                .OrderBy(c => c.Name);
            if (id != null)
            {
                ViewBag.CourseId = id.Value;
                courseView.Students = courseView.Courses.Where(
                    c => c.IdCourse == id.Value).Single().Students;
            }
            return View(courseView);
        }

        public JsonResult GetStudentsEnrolled()
        {
            List<Courses> Courses = db.Courses.ToList();
            List<DataChart> data = new List<DataChart>();
            foreach(var course in Courses)
            {
                int nr = course.Students.Count;
                DataChart aux = new DataChart()
                {
                    Course = course.Name,
                    NumberOfStudents = nr
                };
                data.Add(aux);
            }
            return Json(data);
        }

        public ActionResult Details(int id = 0)
        {
            Courses courses = db.Courses.Find(id);
            if (courses == null)
            {
                return HttpNotFound();
            }
            return View(courses);
        }

        [Authorize(Users = "Simi")]
        public ActionResult Create()
        {
            ViewBag.IdCategory = new SelectList(db.Categories, "IdCategory", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "Simi")]
        public ActionResult Create(Courses courses, HttpPostedFileBase image2)
        {
            if (image2 != null)
            {
                courses.Image = new byte[image2.ContentLength];
                image2.InputStream.Read(courses.Image, 0, image2.ContentLength);
            }
            if (ModelState.IsValid)
            {
                db.Courses.Add(courses);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCategory = new SelectList(db.Categories, "IdCategory", "Name", courses.IdCategory);
            return View(courses);
        }


        [Authorize(Users = "Simi")]
        public ActionResult Edit(int id = 0)
        {
            Courses courses = db.Courses.Find(id);
            if (courses == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdCategory = new SelectList(db.Categories, "IdCategory", "Name", courses.IdCategory);
            return View(courses);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "Simi")]
        public ActionResult Edit(Courses courses, HttpPostedFileBase image2)
        {
            if (image2 != null)
            {
                courses.Image = new byte[image2.ContentLength];
                image2.InputStream.Read(courses.Image, 0, image2.ContentLength);
            }
            if (ModelState.IsValid)
            {
                db.Entry(courses).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCategory = new SelectList(db.Categories, "IdCategory", "Name", courses.IdCategory);
            return View(courses);
        }

        [Authorize(Users = "Simi")]
        public ActionResult Delete(int id = 0)
        {
            Courses courses = db.Courses.Find(id);
            if (courses == null)
            {
                return HttpNotFound();
            }
            return View(courses);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Courses courses = db.Courses.Find(id);
            foreach (var student in courses.Students.ToList())
            {
                courses.Students.Remove(student);
            }
            db.Courses.Remove(courses);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        public class DataChart
        {
            public String Course { get; set; }
            public int NumberOfStudents { get; set; }
        }
    }
}