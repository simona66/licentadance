﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ColoursOfDance.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "";



            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Prezentarea sistemului informatic";

            return View();
        }

       

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Inscriere()
        {
            return View();
        }

        public ActionResult Meniu()
        {
            ViewBag.Message = "Meniu";
            return View();
        }


    }
}
