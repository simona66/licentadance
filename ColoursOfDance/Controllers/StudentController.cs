﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ColoursOfDance.Models;

namespace ColoursOfDance.Controllers
{
    public class StudentController : Controller
    {
        private ColoursOfDanceEntities1 db = new ColoursOfDanceEntities1();

        //
        // GET: /Student/

        public ActionResult Index()
        {
            return View(db.Students.ToList());
        }

        //
        // GET: /Student/Details/5

        public ActionResult Details(int id = 0)
        {
            Students students = db.Students.Find(id);
            if (students == null)
            {
                return HttpNotFound();
            }
            return View(students);
        }

        //
        // GET: /Student/Create

        public ActionResult Create()
        {
            StudentViewModel studentView = new StudentViewModel()
            {
                Student = new Students(),
                Courses = new MultiSelectList(db.Courses, "IdCourse", "Name")
            };
            return View(studentView);
        }

        //
        // POST: /Student/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(StudentViewModel students)
        {
            students.Courses = new MultiSelectList(db.Courses, "IdCourse", "Name");
            if (ModelState.IsValid)
            {
                List<Courses> courses = new List<Courses>();
                foreach (var id in students.selectedCourses)
                {
                    courses.Add(db.Courses.Find(Int32.Parse(id)));
                }
                students.Student.Courses = courses;
                db.Students.Add(students.Student);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            students.Courses = new MultiSelectList(db.Courses, "IdCourse", "Name");
            return View(students);
        }

        //
        // GET: /Student/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Students students = db.Students.Find(id);
            if (students == null)
            {
                return HttpNotFound();
            }
            return View(students);
        }

        //
        // POST: /Student/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Students students)
        {
            if (ModelState.IsValid)
            {
                db.Entry(students).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(students);
        }

        //
        // GET: /Student/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Students students = db.Students.Find(id);
            if (students == null)
            {
                return HttpNotFound();
            }
            return View(students);
        }

        //
        // POST: /Student/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Students students = db.Students.Find(id);
            foreach (var course in students.Courses.ToList())
            {
                students.Courses.Remove(course);
            }
            db.Students.Remove(students);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}