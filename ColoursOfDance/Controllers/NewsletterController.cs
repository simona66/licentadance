﻿using ColoursOfDance.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace ColoursOfDance.Controllers
{
    public class NewsletterController : Controller
    {

        private ColoursOfDanceEntities1 db = new ColoursOfDanceEntities1();

        private void CreateTemplate()
        {
           // FileStream fs = new FileStream(@"C:\Users\Simi\Desktop\Licenta\ColoursOfDance\ColoursOfDance\Views\Newsletter\newsletter.cshtml",FileMode.Create,FileAccess.Write);
            StreamWriter sw = new StreamWriter(@"C:\Users\Simi\Desktop\ColoursOfDance\ColoursOfDance\Views\Newsletter\newsletter.cshtml");

            sw.Flush();
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html><html><head><meta name=\"viewport\" content=\"width=device-width\" /></head><body>");
            foreach(var item in db.Events.ToList())
            {
                if(item.Date >= DateTime.Now)
                {
                    sb.Append("<div style=\"border-style: double; color: white; background-color: #B0BEC5;color: black\">");
                    sb.Append("<h1 style=\"text-align: center;color: black\">" + item.Name + "</h1>");
                    sb.Append("<br/><br /><table><tr><td><img src=" + item.Link + " style=\"width: 500px\"/></td>");
                    sb.Append("<td><table style=\"margin-left:30px\"><tr><td><img style=\"width: 60px\" src=\"http://strikeracademy.co.uk/wp-content/uploads/2014/02/Location-Icon-Blue.png\"/><h3>Locatie</h3></td><td>");
                    sb.Append("<h4 style=\"color: grey\">" + item.Location + "</h4></td></tr>");
                    sb.Append("<tr><td><img style=\"width:60px\" src=\"https://cdn1.iconfinder.com/data/icons/ui-5/502/calendar-512.png\" /><h3>Data</32></td>");
                    sb.Append("<td><h4 style=\"color: grey\">" + item.Date.ToShortDateString() + "</h4></td></tr>");
                    sb.Append("<tr><td><img style=\"width:60px\" src=\"http://icons.iconarchive.com/icons/rade8/minium-2/256/Sidebar-Search-icon.png\" /><h3>Ora</h3></td>");
                    sb.Append("<td><h4 style=\"color: grey\">" + item.StartHour + "</h4></td></tr>");
                    sb.Append("</table></td></tr></table>");
                    sb.Append("</div>");
                
                }
            }
            
            sb.Append("</body></html>");
            sw.WriteLine(sb.ToString());
            sw.Flush();
            sw.Close();
     
        }

        [AllowAnonymous]
        public async Task<ActionResult> SendEmail()
        {
            
            CreateTemplate();
            var message = await EMailTemplate("newsletter");

            foreach(var item in db.Students.ToList())
            {
                await MessageServices.SendEmailAsync(item.Email, "newsletter", message);
            }
            return View("EmailSent");
        }


        [HttpGet]
        [AllowAnonymous]
        public ActionResult EmailSent()
        {
            return View();
        }
        //
        // GET: /Newsletter/

        public ActionResult Index()
        {

            return View();
        }

        public ActionResult newsletter()
        {
            return View(db.Events.ToList());
        }

        public static async Task<string> EMailTemplate(string template)
        {
            var templateFilePath = HostingEnvironment.MapPath("~/Views/Newsletter/") + template + ".cshtml";
            StreamReader objectstreamreaderfile = new StreamReader(templateFilePath);
            var body = await objectstreamreaderfile.ReadToEndAsync();
            objectstreamreaderfile.Close();
            return body;
        }

    }
}
