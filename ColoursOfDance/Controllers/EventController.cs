﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ColoursOfDance.Models;


namespace ColoursOfDance.Controllers
{
    [Authorize]
    public class EventController : Controller
    {
        private ColoursOfDanceEntities1 db = new ColoursOfDanceEntities1();

        //
        // GET: /Event/

        public ActionResult Index()
        {
            return View(db.Events.ToList());
        }

        public ActionResult Upcoming()
        {
            return View(db.Events.ToList());
        }

        //
        // GET: /Event/Details/5

        public ActionResult Details(int id = 0)
        {
            Events events = db.Events.Find(id);
            if (events == null)
            {
                return HttpNotFound();
            }
            return View(events);
        }

        //
        // GET: /Event/Create
        
        [Authorize(Users = "Simi")]
        public ActionResult Create()
        {
            Events e=new Events();
            return View(e);
        }
        
        //
        // POST: /Event/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "Simi")]
        public ActionResult Create(Events events, HttpPostedFileBase image1)
        {
            if(image1!=null)
            {
                events.Image = new byte[image1.ContentLength];
                image1.InputStream.Read(events.Image,0,image1.ContentLength);
            }
            if (ModelState.IsValid)
            {
                db.Events.Add(events);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           
            return View(events);
        }
       


        //
        // GET: /Event/Edit/5
        public ActionResult Edit(int id = 0)
        {
            Events events = db.Events.Find(id);
            if (events == null)
            {
                return HttpNotFound();
            }
            return View(events);
        }

        //
        // POST: /Event/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Events events)
        {
            if (ModelState.IsValid)
            {
                db.Entry(events).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(events);
        }

        //
        // GET: /Event/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Events events = db.Events.Find(id);
            if (events == null)
            {
                return HttpNotFound();
            }
            return View(events);
        }

        //
        // POST: /Event/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Events events = db.Events.Find(id);
            db.Events.Remove(events);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}