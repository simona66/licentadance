﻿using System.Web;
using System.Web.Mvc;

namespace ColoursOfDance
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}