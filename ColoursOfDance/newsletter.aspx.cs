﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ColoursOfDance
{
    public partial class newsletter1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                MailMessage Msg = new MailMessage();
                Msg.From = new MailAddress(ConfigurationManager.AppSettings["Email"]);
                Msg.To.Add(txtEmail.Text);
                StreamReader reader = new StreamReader(Server.MapPath("~/SendMail.htm"));
                string readFile = reader.ReadToEnd();
                string StrContent = "";
                StrContent = readFile;
                //Here replace the name with [MyName] 
                StrContent = StrContent.Replace("[MyName]", txtName.Text);

                Msg.Subject = "Dotnetfox - Thanks for your subscription";
                Msg.Body = StrContent.ToString();
                Msg.IsBodyHtml = true;
                // your remote SMTP server IP.
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["MailServer"];
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["Email"];
                NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["MailPort"]);
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSSLEnabled"].ToString());

                smtp.Send(Msg);
            }
            catch (Exception ex)
            { }
        }
    }
}